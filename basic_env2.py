from env import ArmEnv

env = ArmEnv(n_arms=5)
for _ in range(3000):
    env.render()
    action = env.sample_action()
    env.step(action)
    