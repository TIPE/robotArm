from env import ArmEnv
from rl import DDPG

env = ArmEnv(n_arms=2)
model = DDPG(env.action_dim, env.state_dim, env.action_bound)

# 拿最初的状态
state = env.reset()

# 选动作
action = model.act(state)
print(action)

# 做动作并得到下一个状态与得分
next_state, reward, done = env.step(action)

# 存储这一系列的信息，后面要从这些信息中学习
model.store_transition(state, action, reward, next_state)

# 从收集的所有学习资料中学习一次
model.learn()