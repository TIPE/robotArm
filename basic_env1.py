from env import ArmEnv

env = ArmEnv()
for _ in range(3000):
    env.render()
    action = env.sample_action()
    env.step(action)
