from env import ArmEnv

env = ArmEnv(n_arms=2)
print("动作维度:", env.action_dim)
print("动作范围:", env.action_bound)
print("状态维度:", env.state_dim)


def show_arm_state(num_arms):
    env = ArmEnv(n_arms=num_arms)
    state = env.reset()
    print("每个手臂到达目标的距离:", state[:num_arms])
    print("每个手臂与目标的角度:", state[num_arms:num_arms+num_arms])
    print("是否碰触到目标:", state[-1])

num_arms = 3
show_arm_state(num_arms)

num_arms = 2
show_arm_state(num_arms)
