from env import ArmEnv
from rl import DDPG

env = ArmEnv(n_arms=2)
model = DDPG(env.action_dim, env.state_dim, env.action_bound)
for ep in range(3):
    state = env.reset()
    for t in range(100):
        env.render()
        # 让model来帮我们根据现在的状态(state)来挑选动作(action)
        action = model.act(state)

        # env 的 step 功能其实还会返回下一个状态，奖励，是否完成任务
        next_state, reward, done = env.step(action)

        # 注意，下一个 t 中，我们给模型的状态要是一个新状态
        state = next_state