from env import ArmEnv

env = ArmEnv(n_arms=5, on_mouse=True)
for _ in range(10):
    env.reset()
    for _ in range(500):
        env.render()
        action = env.sample_action()
        env.step(action)
