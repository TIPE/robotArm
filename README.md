# AI 机械手臂

上手实做一个自动学习的AI机械手臂，见证它的自我学习能力。

## 效果预览

![img](img/5arm.png)


## 学习材料

[文档材料](文档)

## 代码

- 依赖代码（只引用，不用修改）
    - [env.py](env.py) 环境依赖代码
    - [rl.py](rl.py) 算法依赖代码
- Demo演示
    - [basic_program.py](basic_program.py)
- 基础环境（练习代码）
    - [basic_env1.py](basic_env1.py)
    - [basic_env2.py](basic_env2.py)
    - [basic_env3.py](basic_env3.py)
    - [basic_env4.py](basic_env4.py)
    - [basic_env5.py](basic_env5.py)
- 人工模拟AI学习（练习代码）
    - [human1.py](human1.py)
    - [human2.py](human2.py)
    - [human3.py](human3.py)
- AI 自主学习（练习代码）
    - [ai1.py](ai1.py)
    - [ai2.py](ai2.py)
    - [ai3.py](ai3.py)
- 保存加载学习成果（练习代码）
    - [restore1.py](restore1.py)
    - [restore2.py](restore2.py)

