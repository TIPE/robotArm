from env import ArmEnv

# 定义环境参数
env = ArmEnv(n_arms=2, random_goal=False, on_mouse=False)
# 运行 10 个回合
for ep in range(10):
    # 每个回合都重新设置一遍初始位置
    s = env.reset()
    # 每回合 100 步
    for j in range(100):
        # 将环境变化展示在显示器上
        env.render()
        # 手臂需要做什么动作?
        env.step(env.sample_action())