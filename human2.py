from env import ArmEnv

env = ArmEnv(n_arms=2, random_goal=False, on_mouse=False, fps=30)
for ep in range(4):
    s = env.reset()
    for j in range(300):
        env.render()
        # 手臂需要做什么动作?你可以通过键盘，手动学习操控机械手臂[1,q,2,w] 等按键调整手臂位置
        env.step()