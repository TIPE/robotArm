from env import ArmEnv
from rl import DDPG


arms = 3
env = ArmEnv(n_arms=arms, on_mouse=True)
model = DDPG(env.action_dim, env.state_dim, env.action_bound)
model.load_weights("models/{}arms/ep{}".format(arms, 1160))
env.render()
env.viewer.set_vsync(True)
s = env.reset()
while True:
    env.render()
    a = model.act(s)
    s, r, done = env.step(a)